import matplotlib.pyplot as plt
import numpy as np
import matplotlib as mpl
from matplotlib.colors import ListedColormap, LinearSegmentedColormap

wavelength = 1030.0e-9
wavenumber = 2.0 * np.pi / wavelength

numerical_aperture = 0.31  # obtained by measuring drawing angle
obscuration = 0.27
focal_length = 13.3e-3

ideal_waist_0 = wavelength / (np.pi * numerical_aperture)
rayleigh_range = np.pi * ideal_waist_0**2 / wavelength
print("Rayleigh Range: " + str(rayleigh_range))

beam_propagation_method = False
if beam_propagation_method:
    x_min = -13.0e-3
    x_max = 13.0e-3
    x_spacing = 0.2e-6
    x_step_count = int((x_max - x_min) / x_spacing)
    x_vals = np.linspace(-13.0e-3, 13.0e-3, 1000)
    print("x spacing: " + str(x_vals[1] - x_vals[0]))
    y_vals = x_vals.copy()
    X, Y = np.meshgrid(x_vals, y_vals, indexing="ij")
    z_min = -32.0e-3
    z_max = 10.0e-3
    z_step_count = int((z_max - z_min) / 0.1e-6)
    z_vals = np.linspace(z_min, z_max, z_step_count)
    dz = z_vals[1] - z_vals[0]

    E_field = np.zeros((x_vals.shape[0], y_vals.shape[0]))


def waist(z: np.ndarray):
    return ideal_waist_0 * np.sqrt(1 + (z / rayleigh_range) ** 2)


def wavefront_radius(z: np.ndarray):
    return z * (1 + (rayleigh_range / z) ** 2)


def radial_phasor(X: np.ndarray, Y: np.ndarray, z: float) -> np.array:
    radius = wavefront_radius(z)
    print("Radius: " + str(radius))
    phase = -1.0j * wavenumber * (X**2 + Y**2) / (2 * radius)
    return np.exp(phase)


if beam_propagation_method:
    fig, ax = plt.subplots()
    # Plot the surface.
    surf = ax.pcolormesh(X, Y, np.angle(radial_phasor(X, Y, z_vals[0])))
    plt.show()


x_vals = np.linspace(-4.0 * rayleigh_range, 4.0 * rayleigh_range, 1000)
y_vals = np.linspace(-10.0e-6, 10.0e-6, 1000)

X, Y = np.meshgrid(x_vals, y_vals, indexing="ij")


def point_source(x_start, y_start, x_grid, y_grid):
    X_shifted = x_grid - x_start
    Y_shifted = y_grid - y_start
    separation = np.sqrt(X_shifted**2 + Y_shifted**2)
    return np.exp(1.0j * wavenumber * separation) / separation


def custom_plot(filename: str, X, Y, Z):
    # Custom Divergent colormap
    cdict = {
        "red": [(0.0, 0.0, 0.0), (0.5, 0.0, 0.0), (1.0, 1.0, 1.0)],
        "green": [(0.0, 0.0, 0.0), (1.0, 0.0, 0.0)],
        "blue": [(0.0, 1.0, 1.0), (0.5, 0.0, 0.0), (1.0, 0.0, 0.0)],
    }
    colormap = LinearSegmentedColormap("Divergent Hot-Cold", cdict, N=256)

    fig, ax = plt.subplots()
    plot = ax.pcolormesh(X, Y, Z, cmap=colormap)  # vmin=-2.5e3, vmax=2.5e3,
    fig.colorbar(plot, ax=ax, label="E Field Amplitue (arb.)")
    ax.set_aspect("equal")
    plt.savefig(filename)
    plt.close()


radius_at_focal_length = wavefront_radius(-focal_length)
max_angle = np.arcsin(numerical_aperture)
print("max angle: " + str(max_angle))
for j in range(2, 30):
    data = np.zeros_like(X, dtype=np.complex128)
    source_count = j
    for i in range(source_count):
        angle = -max_angle + i / (source_count - 1) * (2 * max_angle)
        forward_curve_offset = focal_length * (1.0 - np.cos(angle))
        radial_distance = focal_length * np.sin(angle)
        print("offset: " + str(forward_curve_offset) + "\tangle: " + str(angle))
        data += (
            point_source(-focal_length + forward_curve_offset, radial_distance, X, Y)
            / source_count
        )

    custom_plot(f"images/{j:02}.png", X, Y, data.real)
